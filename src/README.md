# SRC

  src is traditionally the quick way of saying 'source, as in the source code of of a project, and Song Island follows this tradition.  The files here are what make this particular song island run the way it does.  However, a song island doesn't reference any of these files directly.  Instead, the entirety of this folder is bundled and compressed into a few small files, and stored in our [admin](../admin) folder.  This makes the site run quickly and reliably, and lets us write in a javascript style that we prefer, that then gets compiled into javscript the browser prefers.  

## GOOD TO KNOW

  What this means for you is that you can read through and mess around with the files here, but it won't affect the site.  And if you _do_ want to change the underlying structure and code of a song island, then you are going to want to clone this folder to your computer, run it in a dev environment, and then build the new bundle files.

## The Developing Environment for Song Island

This song island is written mostly with node javascript, using a framework called [Svelte](https://svelte.dev).  Svelte has a great tutorial, with interactive code samples, and it is written to be focused sections of css, html, and javascript.  In other words, the code here flows naturally from what you'd learn by making a simple website (which I heartily recommend you do!)

To work on the code, you will want to copy it down to your computer (which you can do using the settings tab at the top of this page).  Then, using your terminal, navigate to the folder, and then start up a dev environment with npm.  

```shell
cd ~/Place/You/Saved/These/Files
npm run dev
```

So to do this means you need to:
- have a terminal
- have node and its package manager npm installed.

If you are on a mac, linux, or windows10, the terminal is already installed on your machine.  For earlier versions of windows, you want to look up and install a program called `puTTY`.

You can check if you already have node installed on your computer by typing in your terminal
```
node --version
```

it should return something like `v9.10.1` (the exact version number don't matter too much).  If you get a `command not found`, you will need to install node.  You can do that here: https://nodejs.org/en/

Once installed, run `node --version` to confirm you have it.  npm comes with node, so you can now run the `npm run dev ` command listed above.

Then, use a text editor (including the one built into beaker) to adjust the files, and check out your changes in the preview mode of beaker (toggle-able in the settings).
When done, you can type in your terminal

```
npm run build
```

to re-bundle all the code, and then in beaker publish your changes, and they will go into affect.

## Encouragement
This whole section might sound like a lot, because it is-- you're setting up a whole coding environment through the command line to work with cutting-edge decentralized protocols.  If this is new for you, I encourage you to go at your own pace and get comfy with each component.  Maybe just open up the command line and try to move around and list files until that feels great, then move to installing node, then trying to edit a basic website before going through the svelte tutorial.  Each one of these parts can introduce a whole lifetime of discovery and enjoyment and let you craft incredible things, so enjoy the side roads.

I wrote up this (admittedly scattered) intro because I wanted you, specifically, to enjoy coding and crafting this island.  If there's any way I can help you get started, feel free to reach out at webmaster@coolguy.website
