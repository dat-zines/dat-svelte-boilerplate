import { writable, derived } from 'svelte/store';
import { isEmpty } from 'lodash-es';

export const archive = writable({});

export const isOwner = derived(
  archive,
  async($arch, set)  => {
    if (isEmpty($arch)) {
      set(false);
    } else {
      $arch.getInfo()
        .then(info => set(info.isOwner))
    }
  }
);
