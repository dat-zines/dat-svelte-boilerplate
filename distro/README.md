# D I S T R O  

Distros are an essential aspect of zine culture.  Distros collect and distribute zines.  They are often run by a small amount of people (or just one person), and are mythically important but materially small (the distro that changes your life may just be a box in a closet of a rented room, and run by one passionate zinester.)

In the same way, customizing your dat zine on your own computer is delightful, but limiting.  You want to _share_ your work too, and give guidance on how it's shared.

The `distro` folder is intended to give instructions for when this zine is found and shared by dat-zine distros and libraries.  It contains an info.txt page and can also include a cover image (title cover.jpg, or cover.gif, or cover.png).  I didn't want to assume the background image for the island was _also_ a cover image, so it's not made autmoatically.

The info.zine file is formatted like your songIsland.zine file, though youc an be more fast and loose with the structure here.



## What _should_ I put in my info.zine

While it can be anything, what it _should_ be is based on the community you're sharing it within! This is why I've setup moods and RIYL by default, assuming you'll pass this to folks who want to be able to organize all their songs by these two elements.
You know your community better than me, though, so please extend the fields in the file to fit whatever helps yr local zine librarian/song island station to share what this good thing.

## Examples of Real-Life Zine Distros
My absolute favorite distro is [Antiquated Futures](http://www.antiquatedfuture.com/) AND they have a great link list of other distros.  You should really just check out that!
[AF's list of Zine Distros](http://www.antiquatedfuture.com/links/)
